//
// Created by the DataSnap proxy generator.
// 02/06/2018 12:40:45
//

unit ClientClasses;

interface

uses System.JSON, Datasnap.DSProxyRest, Datasnap.DSClientRest, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.FireDACJSONReflect, Data.DBXJSONReflect;

type

  IDSRestCachedTFDJSONDataSets = interface;

  TServerMethodClient = class(TDSAdminRestClient)
  private
    FEchoStringCommand: TDSRestCommand;
    FReverseStringCommand: TDSRestCommand;
    FInsertprocDJSONCommand: TDSRestCommand;
    FBuscaJSCommand: TDSRestCommand;
    FBuscaJSCommand_Cache: TDSRestCommand;
    FInsertJSCommand: TDSRestCommand;
    FBorrarRegistroCommand: TDSRestCommand;
    FBuscaDJSCommand: TDSRestCommand;
    FBuscaDJSCommand_Cache: TDSRestCommand;
    Factivar_deviceCommand: TDSRestCommand;
    FInsertDeviceCommand: TDSRestCommand;
  public
    constructor Create(ARestConnection: TDSRestConnection); overload;
    constructor Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function EchoString(Value: string; const ARequestFilter: string = ''): string;
    function ReverseString(Value: string; const ARequestFilter: string = ''): string;
    function InsertprocDJSON(proc_name: string; Registros: TFDJSONDataSets; const ARequestFilter: string = ''): Integer;
    function BuscaJS(query: string; const ARequestFilter: string = ''): TJSONArray;
    function BuscaJS_Cache(query: string; const ARequestFilter: string = ''): IDSRestCachedJSONArray;
    function InsertJS(Tabla: string; Registro: TJSONArray; const ARequestFilter: string = ''): string;
    function BorrarRegistro(Tabla: string; Where: string; const ARequestFilter: string = ''): string;
    function BuscaDJS(query: string; const ARequestFilter: string = ''): TFDJSONDataSets;
    function BuscaDJS_Cache(query: string; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function activar_device(deviceid: string; const ARequestFilter: string = ''): string;
    function InsertDevice(deviceid: string; comp_id: string; suc_id: string; const ARequestFilter: string = ''): string;
  end;

  IDSRestCachedTFDJSONDataSets = interface(IDSRestCachedObject<TFDJSONDataSets>)
  end;

  TDSRestCachedTFDJSONDataSets = class(TDSRestCachedObject<TFDJSONDataSets>, IDSRestCachedTFDJSONDataSets, IDSRestCachedCommand)
  end;

const
  TServerMethod_EchoString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethod_ReverseString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethod_InsertprocDJSON: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'proc_name'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'Registros'; Direction: 1; DBXType: 37; TypeName: 'TFDJSONDataSets'),
    (Name: ''; Direction: 4; DBXType: 6; TypeName: 'Integer')
  );

  TServerMethod_BuscaJS: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'query'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TJSONArray')
  );

  TServerMethod_BuscaJS_Cache: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'query'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethod_InsertJS: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'Tabla'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'Registro'; Direction: 1; DBXType: 37; TypeName: 'TJSONArray'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethod_BorrarRegistro: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'Tabla'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'Where'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethod_BuscaDJS: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'query'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethod_BuscaDJS_Cache: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'query'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethod_activar_device: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'deviceid'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethod_InsertDevice: array [0..3] of TDSRestParameterMetaData =
  (
    (Name: 'deviceid'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'comp_id'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'suc_id'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

implementation

function TServerMethodClient.EchoString(Value: string; const ARequestFilter: string): string;
begin
  if FEchoStringCommand = nil then
  begin
    FEchoStringCommand := FConnection.CreateCommand;
    FEchoStringCommand.RequestType := 'GET';
    FEchoStringCommand.Text := 'TServerMethod.EchoString';
    FEchoStringCommand.Prepare(TServerMethod_EchoString);
  end;
  FEchoStringCommand.Parameters[0].Value.SetWideString(Value);
  FEchoStringCommand.Execute(ARequestFilter);
  Result := FEchoStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethodClient.ReverseString(Value: string; const ARequestFilter: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FConnection.CreateCommand;
    FReverseStringCommand.RequestType := 'GET';
    FReverseStringCommand.Text := 'TServerMethod.ReverseString';
    FReverseStringCommand.Prepare(TServerMethod_ReverseString);
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.Execute(ARequestFilter);
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethodClient.InsertprocDJSON(proc_name: string; Registros: TFDJSONDataSets; const ARequestFilter: string): Integer;
begin
  if FInsertprocDJSONCommand = nil then
  begin
    FInsertprocDJSONCommand := FConnection.CreateCommand;
    FInsertprocDJSONCommand.RequestType := 'POST';
    FInsertprocDJSONCommand.Text := 'TServerMethod."InsertprocDJSON"';
    FInsertprocDJSONCommand.Prepare(TServerMethod_InsertprocDJSON);
  end;
  FInsertprocDJSONCommand.Parameters[0].Value.SetWideString(proc_name);
  if not Assigned(Registros) then
    FInsertprocDJSONCommand.Parameters[1].Value.SetNull
  else
  begin
    FMarshal := TDSRestCommand(FInsertprocDJSONCommand.Parameters[1].ConnectionHandler).GetJSONMarshaler;
    try
      FInsertprocDJSONCommand.Parameters[1].Value.SetJSONValue(FMarshal.Marshal(Registros), True);
      if FInstanceOwner then
        Registros.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FInsertprocDJSONCommand.Execute(ARequestFilter);
  Result := FInsertprocDJSONCommand.Parameters[2].Value.GetInt32;
end;

function TServerMethodClient.BuscaJS(query: string; const ARequestFilter: string): TJSONArray;
begin
  if FBuscaJSCommand = nil then
  begin
    FBuscaJSCommand := FConnection.CreateCommand;
    FBuscaJSCommand.RequestType := 'GET';
    FBuscaJSCommand.Text := 'TServerMethod.BuscaJS';
    FBuscaJSCommand.Prepare(TServerMethod_BuscaJS);
  end;
  FBuscaJSCommand.Parameters[0].Value.SetWideString(query);
  FBuscaJSCommand.Execute(ARequestFilter);
  Result := TJSONArray(FBuscaJSCommand.Parameters[1].Value.GetJSONValue(FInstanceOwner));
end;

function TServerMethodClient.BuscaJS_Cache(query: string; const ARequestFilter: string): IDSRestCachedJSONArray;
begin
  if FBuscaJSCommand_Cache = nil then
  begin
    FBuscaJSCommand_Cache := FConnection.CreateCommand;
    FBuscaJSCommand_Cache.RequestType := 'GET';
    FBuscaJSCommand_Cache.Text := 'TServerMethod.BuscaJS';
    FBuscaJSCommand_Cache.Prepare(TServerMethod_BuscaJS_Cache);
  end;
  FBuscaJSCommand_Cache.Parameters[0].Value.SetWideString(query);
  FBuscaJSCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedJSONArray.Create(FBuscaJSCommand_Cache.Parameters[1].Value.GetString);
end;

function TServerMethodClient.InsertJS(Tabla: string; Registro: TJSONArray; const ARequestFilter: string): string;
begin
  if FInsertJSCommand = nil then
  begin
    FInsertJSCommand := FConnection.CreateCommand;
    FInsertJSCommand.RequestType := 'POST';
    FInsertJSCommand.Text := 'TServerMethod."InsertJS"';
    FInsertJSCommand.Prepare(TServerMethod_InsertJS);
  end;
  FInsertJSCommand.Parameters[0].Value.SetWideString(Tabla);
  FInsertJSCommand.Parameters[1].Value.SetJSONValue(Registro, FInstanceOwner);
  FInsertJSCommand.Execute(ARequestFilter);
  Result := FInsertJSCommand.Parameters[2].Value.GetWideString;
end;

function TServerMethodClient.BorrarRegistro(Tabla: string; Where: string; const ARequestFilter: string): string;
begin
  if FBorrarRegistroCommand = nil then
  begin
    FBorrarRegistroCommand := FConnection.CreateCommand;
    FBorrarRegistroCommand.RequestType := 'GET';
    FBorrarRegistroCommand.Text := 'TServerMethod.BorrarRegistro';
    FBorrarRegistroCommand.Prepare(TServerMethod_BorrarRegistro);
  end;
  FBorrarRegistroCommand.Parameters[0].Value.SetWideString(Tabla);
  FBorrarRegistroCommand.Parameters[1].Value.SetWideString(Where);
  FBorrarRegistroCommand.Execute(ARequestFilter);
  Result := FBorrarRegistroCommand.Parameters[2].Value.GetWideString;
end;

function TServerMethodClient.BuscaDJS(query: string; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FBuscaDJSCommand = nil then
  begin
    FBuscaDJSCommand := FConnection.CreateCommand;
    FBuscaDJSCommand.RequestType := 'GET';
    FBuscaDJSCommand.Text := 'TServerMethod.BuscaDJS';
    FBuscaDJSCommand.Prepare(TServerMethod_BuscaDJS);
  end;
  FBuscaDJSCommand.Parameters[0].Value.SetWideString(query);
  FBuscaDJSCommand.Execute(ARequestFilter);
  if not FBuscaDJSCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FBuscaDJSCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FBuscaDJSCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FBuscaDJSCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethodClient.BuscaDJS_Cache(query: string; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FBuscaDJSCommand_Cache = nil then
  begin
    FBuscaDJSCommand_Cache := FConnection.CreateCommand;
    FBuscaDJSCommand_Cache.RequestType := 'GET';
    FBuscaDJSCommand_Cache.Text := 'TServerMethod.BuscaDJS';
    FBuscaDJSCommand_Cache.Prepare(TServerMethod_BuscaDJS_Cache);
  end;
  FBuscaDJSCommand_Cache.Parameters[0].Value.SetWideString(query);
  FBuscaDJSCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FBuscaDJSCommand_Cache.Parameters[1].Value.GetString);
end;

function TServerMethodClient.activar_device(deviceid: string; const ARequestFilter: string): string;
begin
  if Factivar_deviceCommand = nil then
  begin
    Factivar_deviceCommand := FConnection.CreateCommand;
    Factivar_deviceCommand.RequestType := 'GET';
    Factivar_deviceCommand.Text := 'TServerMethod.activar_device';
    Factivar_deviceCommand.Prepare(TServerMethod_activar_device);
  end;
  Factivar_deviceCommand.Parameters[0].Value.SetWideString(deviceid);
  Factivar_deviceCommand.Execute(ARequestFilter);
  Result := Factivar_deviceCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethodClient.InsertDevice(deviceid: string; comp_id: string; suc_id: string; const ARequestFilter: string): string;
begin
  if FInsertDeviceCommand = nil then
  begin
    FInsertDeviceCommand := FConnection.CreateCommand;
    FInsertDeviceCommand.RequestType := 'GET';
    FInsertDeviceCommand.Text := 'TServerMethod.InsertDevice';
    FInsertDeviceCommand.Prepare(TServerMethod_InsertDevice);
  end;
  FInsertDeviceCommand.Parameters[0].Value.SetWideString(deviceid);
  FInsertDeviceCommand.Parameters[1].Value.SetWideString(comp_id);
  FInsertDeviceCommand.Parameters[2].Value.SetWideString(suc_id);
  FInsertDeviceCommand.Execute(ARequestFilter);
  Result := FInsertDeviceCommand.Parameters[3].Value.GetWideString;
end;

constructor TServerMethodClient.Create(ARestConnection: TDSRestConnection);
begin
  inherited Create(ARestConnection);
end;

constructor TServerMethodClient.Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ARestConnection, AInstanceOwner);
end;

destructor TServerMethodClient.Destroy;
begin
  FEchoStringCommand.DisposeOf;
  FReverseStringCommand.DisposeOf;
  FInsertprocDJSONCommand.DisposeOf;
  FBuscaJSCommand.DisposeOf;
  FBuscaJSCommand_Cache.DisposeOf;
  FInsertJSCommand.DisposeOf;
  FBorrarRegistroCommand.DisposeOf;
  FBuscaDJSCommand.DisposeOf;
  FBuscaDJSCommand_Cache.DisposeOf;
  Factivar_deviceCommand.DisposeOf;
  FInsertDeviceCommand.DisposeOf;
  inherited;
end;

end.

