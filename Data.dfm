object dm: Tdm
  OldCreateOrder = False
  Height = 444
  Width = 493
  object FDStanStorageBinLink1: TFDStanStorageBinLink
    Left = 248
    Top = 104
  end
  object DbConnection: TFDConnection
    Params.Strings = (
      'Database=E:\AppMenos\database\database.sdb'
      'DriverID=SQLite')
    LoginPrompt = False
    AfterConnect = DbConnectionAfterConnect
    BeforeConnect = DbConnectionBeforeConnect
    Left = 165
    Top = 16
  end
  object HostTable: TFDQuery
    Connection = DbConnection
    SQL.Strings = (
      'SELECT * FROM Host')
    Left = 61
    Top = 72
  end
  object FixedrulesTable: TFDQuery
    Connection = DbConnection
    SQL.Strings = (
      'select * from fixedRules')
    Left = 59
    Top = 137
    object FixedrulesTableser_no: TIntegerField
      FieldName = 'ser_no'
      Origin = 'ser_no'
    end
    object FixedrulesTablecomp_id: TIntegerField
      FieldName = 'comp_id'
      Origin = 'comp_id'
    end
    object FixedrulesTablesuc_id: TIntegerField
      FieldName = 'suc_id'
      Origin = 'suc_id'
    end
    object FixedrulesTabledevice_phone_number: TStringField
      FieldName = 'device_phone_number'
      Origin = 'device_phone_number'
      Size = 15
    end
    object FixedrulesTableincoming_text: TStringField
      FieldName = 'incoming_text'
      Origin = 'incoming_text'
      Size = 1000
    end
    object FixedrulesTablesearch_atbegin: TBooleanField
      FieldName = 'search_atbegin'
      Origin = 'search_atbegin'
    end
    object FixedrulesTablesearch_atend: TBooleanField
      FieldName = 'search_atend'
      Origin = 'search_atend'
    end
    object FixedrulesTablesearch_inbetween: TBooleanField
      FieldName = 'search_inbetween'
      Origin = 'search_inbetween'
    end
    object FixedrulesTablesend_text: TStringField
      FieldName = 'send_text'
      Origin = 'send_text'
      Size = 4000
    end
    object FixedrulesTableprocedure_name: TStringField
      FieldName = 'procedure_name'
      Origin = 'procedure_name'
      Size = 500
    end
    object FixedrulesTablewait_minutes: TIntegerField
      FieldName = 'wait_minutes'
      Origin = 'wait_minutes'
    end
    object FixedrulesTablereceiver: TStringField
      FieldName = 'receiver'
      Origin = 'receiver'
      Size = 1
    end
    object FixedrulesTablespecific_contacts: TStringField
      FieldName = 'specific_contacts'
      Origin = 'specific_contacts'
      Size = 4000
    end
    object FixedrulesTablespecific_groups: TStringField
      FieldName = 'specific_groups'
      Origin = 'specific_groups'
      Size = 4000
    end
    object FixedrulesTablespecific_hours: TStringField
      FieldName = 'specific_hours'
      Origin = 'specific_hours'
      Size = 4000
    end
    object FixedrulesTablespecific_days: TStringField
      FieldName = 'specific_days'
      Origin = 'specific_days'
      Size = 4000
    end
    object FixedrulesTableactive: TBooleanField
      FieldName = 'active'
      Origin = 'active'
    end
    object FixedrulesTablebegin_date: TDateTimeField
      FieldName = 'begin_date'
      Origin = 'begin_date'
    end
    object FixedrulesTableend_date: TDateTimeField
      FieldName = 'end_date'
      Origin = 'end_date'
    end
    object FixedrulesTableadd_date: TDateTimeField
      FieldName = 'add_date'
      Origin = 'add_date'
    end
    object FixedrulesTablemedia_url: TStringField
      FieldName = 'media_url'
      Origin = 'media_url'
      Size = 1000
    end
    object FixedrulesTableMEDIA_NAME: TStringField
      FieldName = 'MEDIA_NAME'
      Origin = 'MEDIA_NAME'
      Size = 500
    end
    object FixedrulesTableSTART_OUT_SERVICE_TIME: TTimeField
      FieldName = 'START_OUT_SERVICE_TIME'
      Origin = 'START_OUT_SERVICE_TIME'
    end
    object FixedrulesTableEND_OUT_SERVICE_TIME: TTimeField
      FieldName = 'END_OUT_SERVICE_TIME'
      Origin = 'END_OUT_SERVICE_TIME'
    end
  end
  object Qutils: TFDQuery
    Connection = DbConnection
    Left = 248
    Top = 192
  end
  object RestdbTable: TFDQuery
    Connection = DbConnection
    SQL.Strings = (
      'SELECT * FROM restdb')
    Left = 53
    Top = 311
    object RestdbTableserver: TStringField
      FieldName = 'server'
      Origin = 'server'
      Size = 15
    end
    object RestdbTableport: TIntegerField
      FieldName = 'port'
      Origin = 'port'
    end
    object RestdbTableactivado: TBooleanField
      FieldName = 'activado'
      Origin = 'activado'
    end
  end
  object DevicesTable: TFDQuery
    Connection = DbConnection
    SQL.Strings = (
      'SELECT * FROM devices')
    Left = 58
    Top = 218
    object DevicesTableser_no: TIntegerField
      FieldName = 'ser_no'
      Origin = 'ser_no'
    end
    object DevicesTablecomp_id: TIntegerField
      FieldName = 'comp_id'
      Origin = 'comp_id'
    end
    object DevicesTablesuc_id: TIntegerField
      FieldName = 'suc_id'
      Origin = 'suc_id'
    end
    object DevicesTabledevice_id: TStringField
      FieldName = 'device_id'
      Origin = 'device_id'
      Size = 100
    end
    object DevicesTabledevice_phone: TStringField
      FieldName = 'device_phone'
      Origin = 'device_phone'
    end
    object DevicesTableemail: TStringField
      FieldName = 'email'
      Origin = 'email'
      Size = 500
    end
    object DevicesTableactivation_key: TStringField
      FieldName = 'activation_key'
      Origin = 'activation_key'
      Size = 4000
    end
    object DevicesTabledevice_enabled: TBooleanField
      FieldName = 'device_enabled'
      Origin = 'device_enabled'
    end
    object DevicesTableadd_date: TDateTimeField
      FieldName = 'add_date'
      Origin = 'add_date'
    end
    object DevicesTableregistration_date: TDateTimeField
      FieldName = 'registration_date'
      Origin = 'registration_date'
    end
    object DevicesTabletime_scan: TIntegerField
      FieldName = 'time_scan'
      Origin = 'time_scan'
    end
    object DevicesTablebotonx: TIntegerField
      FieldName = 'botonx'
      Origin = 'botonx'
    end
    object DevicesTablebotony: TIntegerField
      FieldName = 'botony'
      Origin = 'botony'
    end
    object DevicesTablemessageid: TIntegerField
      FieldName = 'messageid'
      Origin = 'messageid'
    end
  end
end
