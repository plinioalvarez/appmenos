program Host;

uses
  System.StartUpCopy,
  FMX.Forms,
  Principal in 'Principal.pas' {FPrincipal},
  Data in 'Data.pas' {dm: TDataModule},
  ClientClasses in 'ClientClasses.pas',
  ClientModule in 'ClientModule.pas' {ClientModule1: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.FormFactor.Orientations := [TFormOrientation.Portrait];
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TClientModule1, ClientModule1);
  Application.CreateForm(TFPrincipal, FPrincipal);
  Application.Run;
end.
