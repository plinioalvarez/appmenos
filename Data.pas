unit Data;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.SQLite,
  FireDAC.Phys.SQLiteDef, FireDAC.Stan.ExprFuncs, FireDAC.FMXUI.Wait, Data.DB,
  FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Stan.StorageBin, System.IOUtils;

type
  Tdm = class(TDataModule)
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    DbConnection: TFDConnection;
    HostTable: TFDQuery;
    FixedrulesTable: TFDQuery;
    Qutils: TFDQuery;
    RestdbTable: TFDQuery;
    RestdbTableserver: TStringField;
    RestdbTableport: TIntegerField;
    RestdbTableactivado: TBooleanField;
    DevicesTable: TFDQuery;
    DevicesTableser_no: TIntegerField;
    DevicesTablecomp_id: TIntegerField;
    DevicesTablesuc_id: TIntegerField;
    DevicesTabledevice_id: TStringField;
    DevicesTabledevice_phone: TStringField;
    DevicesTableemail: TStringField;
    DevicesTableactivation_key: TStringField;
    DevicesTabledevice_enabled: TBooleanField;
    DevicesTableadd_date: TDateTimeField;
    DevicesTableregistration_date: TDateTimeField;
    DevicesTabletime_scan: TIntegerField;
    DevicesTablebotonx: TIntegerField;
    DevicesTablebotony: TIntegerField;
    DevicesTablemessageid: TIntegerField;
    FixedrulesTableser_no: TIntegerField;
    FixedrulesTablecomp_id: TIntegerField;
    FixedrulesTablesuc_id: TIntegerField;
    FixedrulesTabledevice_phone_number: TStringField;
    FixedrulesTableincoming_text: TStringField;
    FixedrulesTablesearch_atbegin: TBooleanField;
    FixedrulesTablesearch_atend: TBooleanField;
    FixedrulesTablesearch_inbetween: TBooleanField;
    FixedrulesTablesend_text: TStringField;
    FixedrulesTableprocedure_name: TStringField;
    FixedrulesTablewait_minutes: TIntegerField;
    FixedrulesTablereceiver: TStringField;
    FixedrulesTablespecific_contacts: TStringField;
    FixedrulesTablespecific_groups: TStringField;
    FixedrulesTablespecific_hours: TStringField;
    FixedrulesTablespecific_days: TStringField;
    FixedrulesTableactive: TBooleanField;
    FixedrulesTablebegin_date: TDateTimeField;
    FixedrulesTableend_date: TDateTimeField;
    FixedrulesTableadd_date: TDateTimeField;
    FixedrulesTablemedia_url: TStringField;
    FixedrulesTableMEDIA_NAME: TStringField;
    FixedrulesTableSTART_OUT_SERVICE_TIME: TTimeField;
    FixedrulesTableEND_OUT_SERVICE_TIME: TTimeField;
    procedure DbConnectionBeforeConnect(Sender: TObject);
    procedure DbConnectionAfterConnect(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dm: Tdm;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

procedure Tdm.DbConnectionAfterConnect(Sender: TObject);
begin

  dbConnection.ExecSQL(
  'CREATE TABLE IF NOT EXISTS [fixedrules] ( '+
  '[ser_no] int,                             '+
  '[comp_id] int NULL,                       '+
  '[suc_id] int NULL,                        '+
  '[device_phone_number] varchar(15) NULL,   '+
  '[incoming_text] varchar(1000)  NULL,      '+
  '[search_atbegin] bit NULL,                '+
  '[search_atend] bit NULL,                  '+
  '[search_inbetween] bit NULL,              '+
  '[send_text] varchar(4000)  NULL,          '+
  '[procedure_name] varchar(500) NULL,       '+
  '[wait_minutes] int NULL,                  '+
  '[receiver] varchar(1)  NULL,              '+
  '[specific_contacts] varchar(4000)  NULL,  '+
  '[specific_groups] varchar(4000) NULL,     '+
  '[specific_hours] varchar(4000)  NULL,     '+
  '[specific_days] varchar(4000)  NULL,      '+
  '[active] bit NULL,                        '+
  '[begin_date] datetime NULL,               '+
  '[end_date] datetime NULL,                 '+
  '[add_date] datetime  NULL,                '+
  '[media_url] varchar(1000) NULL,           '+
  '[MEDIA_NAME] varchar(500) NULL,           '+
  '[START_OUT_SERVICE_TIME] time(7) NULL,    '+
  '[END_OUT_SERVICE_TIME] time(7) NULL       '+
  ' )'  );

  dbConnection.ExecSQL(
  'CREATE TABLE IF NOT EXISTS [Host] (    '+
  '  [servidor]   varchar(20),            '+
  '  [basededatos] varchar(20),           '+
  '  [UltCambio] datetime                 '+
  ' )'  );

  dbConnection.ExecSQL(
   'CREATE TABLE IF NOT EXISTS [devices](  '+
   ' [ser_no] int,                       '+
   ' [comp_id] int,                      '+
   ' [suc_id] int,                       '+
   ' [device_id] varchar(100),           '+
   ' [device_phone] varchar(20),         '+
   ' [email] varchar(500),               '+
   ' [activation_key] varchar(500),     '+
   ' [device_enabled] bit,               '+
   ' [add_date] datetime,                '+
   ' [registration_date] datetime,       '+
   ' [time_scan] int,                    '+
   ' [botonx] int,                       '+
   ' [botony] int,                       '+
   ' [messageid] int                     '+
   ' )'  );

   dbConnection.ExecSQL(
   'CREATE TABLE IF NOT EXISTS [restdb]  ('+
   ' [server] varchar(15),                '+
   ' [port] int,                          '+
   ' [activado] bit                       '+
   ' )'  );


end;

procedure Tdm.DbConnectionBeforeConnect(Sender: TObject);
begin
 {$IF DEFINED(ANDROID)}
   dbConnection.Params.Values['Database'] :=  TPath.Combine(TPath.GetDocumentsPath,'database.sdb');
 {$ENDIF}
end;

end.
