//
// Created by the DataSnap proxy generator.
// 15/06/2018 13:51:21
//

unit ClientClasses;

interface

uses System.JSON, Datasnap.DSProxyRest, Datasnap.DSClientRest, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.FireDACJSONReflect, Data.DBXJSONReflect;

type

  IDSRestCachedTFDJSONDataSets = interface;

  TServerMethodClient = class(TDSAdminRestClient)
  private
    FEchoStringCommand: TDSRestCommand;
    FReverseStringCommand: TDSRestCommand;
    FInsertprocDJSONCommand: TDSRestCommand;
    FBuscaJSCommand: TDSRestCommand;
    FBuscaJSCommand_Cache: TDSRestCommand;
    FInsertJSCommand: TDSRestCommand;
    FBorrarRegistroCommand: TDSRestCommand;
    FBuscaDJSCommand: TDSRestCommand;
    FBuscaDJSCommand_Cache: TDSRestCommand;
    Factivar_deviceCommand: TDSRestCommand;
    FInsertDeviceCommand: TDSRestCommand;
    FwssendmessageCommand: TDSRestCommand;
    FwssendmessageCommand_Cache: TDSRestCommand;
    Fsp_wmdelphiCommand: TDSRestCommand;
  public
    constructor Create(ARestConnection: TDSRestConnection); overload;
    constructor Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function EchoString(Value: string; const ARequestFilter: string = ''): string;
    function ReverseString(Value: string; const ARequestFilter: string = ''): string;
    function InsertprocDJSON(proc_name: string; Registros: TFDJSONDataSets; const ARequestFilter: string = ''): Integer;
    function BuscaJS(query: string; const ARequestFilter: string = ''): TJSONArray;
    function BuscaJS_Cache(query: string; const ARequestFilter: string = ''): IDSRestCachedJSONArray;
    function InsertJS(Tabla: string; Registro: TJSONArray; const ARequestFilter: string = ''): string;
    function BorrarRegistro(Tabla: string; Where: string; const ARequestFilter: string = ''): string;
    function BuscaDJS(query: string; const ARequestFilter: string = ''): TFDJSONDataSets;
    function BuscaDJS_Cache(query: string; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function activar_device(deviceid: string; const ARequestFilter: string = ''): string;
    function InsertDevice(deviceid: string; const ARequestFilter: string = ''): string;
    function wssendmessage(comp_id: Integer; suc_id: Integer; const ARequestFilter: string = ''): TFDJSONDataSets;
    function wssendmessage_Cache(comp_id: Integer; suc_id: Integer; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function sp_wmdelphi(comp_id: Integer; suc_id: Integer; key_from_me: Integer; message_id: Integer; key_remote_jid: string; data: string; media_wa_type: string; media_name: string; media_caption: string; raw_data: TStream; timestamp: TDateTime; lat: Double; lon: Double; remote_resource: string; const ARequestFilter: string = ''): Integer;
  end;

  IDSRestCachedTFDJSONDataSets = interface(IDSRestCachedObject<TFDJSONDataSets>)
  end;

  TDSRestCachedTFDJSONDataSets = class(TDSRestCachedObject<TFDJSONDataSets>, IDSRestCachedTFDJSONDataSets, IDSRestCachedCommand)
  end;

const
  TServerMethod_EchoString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethod_ReverseString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethod_InsertprocDJSON: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'proc_name'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'Registros'; Direction: 1; DBXType: 37; TypeName: 'TFDJSONDataSets'),
    (Name: ''; Direction: 4; DBXType: 6; TypeName: 'Integer')
  );

  TServerMethod_BuscaJS: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'query'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TJSONArray')
  );

  TServerMethod_BuscaJS_Cache: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'query'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethod_InsertJS: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'Tabla'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'Registro'; Direction: 1; DBXType: 37; TypeName: 'TJSONArray'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethod_BorrarRegistro: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'Tabla'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'Where'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethod_BuscaDJS: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'query'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethod_BuscaDJS_Cache: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'query'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethod_activar_device: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'deviceid'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethod_InsertDevice: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'deviceid'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethod_wssendmessage: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'comp_id'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'suc_id'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethod_wssendmessage_Cache: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'comp_id'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'suc_id'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethod_sp_wmdelphi: array [0..14] of TDSRestParameterMetaData =
  (
    (Name: 'comp_id'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'suc_id'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'key_from_me'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'message_id'; Direction: 1; DBXType: 6; TypeName: 'Integer'),
    (Name: 'key_remote_jid'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'data'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'media_wa_type'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'media_name'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'media_caption'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'raw_data'; Direction: 1; DBXType: 33; TypeName: 'TStream'),
    (Name: 'timestamp'; Direction: 1; DBXType: 11; TypeName: 'TDateTime'),
    (Name: 'lat'; Direction: 1; DBXType: 7; TypeName: 'Double'),
    (Name: 'lon'; Direction: 1; DBXType: 7; TypeName: 'Double'),
    (Name: 'remote_resource'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 6; TypeName: 'Integer')
  );

implementation

function TServerMethodClient.EchoString(Value: string; const ARequestFilter: string): string;
begin
  if FEchoStringCommand = nil then
  begin
    FEchoStringCommand := FConnection.CreateCommand;
    FEchoStringCommand.RequestType := 'GET';
    FEchoStringCommand.Text := 'TServerMethod.EchoString';
    FEchoStringCommand.Prepare(TServerMethod_EchoString);
  end;
  FEchoStringCommand.Parameters[0].Value.SetWideString(Value);
  FEchoStringCommand.Execute(ARequestFilter);
  Result := FEchoStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethodClient.ReverseString(Value: string; const ARequestFilter: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FConnection.CreateCommand;
    FReverseStringCommand.RequestType := 'GET';
    FReverseStringCommand.Text := 'TServerMethod.ReverseString';
    FReverseStringCommand.Prepare(TServerMethod_ReverseString);
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.Execute(ARequestFilter);
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethodClient.InsertprocDJSON(proc_name: string; Registros: TFDJSONDataSets; const ARequestFilter: string): Integer;
begin
  if FInsertprocDJSONCommand = nil then
  begin
    FInsertprocDJSONCommand := FConnection.CreateCommand;
    FInsertprocDJSONCommand.RequestType := 'POST';
    FInsertprocDJSONCommand.Text := 'TServerMethod."InsertprocDJSON"';
    FInsertprocDJSONCommand.Prepare(TServerMethod_InsertprocDJSON);
  end;
  FInsertprocDJSONCommand.Parameters[0].Value.SetWideString(proc_name);
  if not Assigned(Registros) then
    FInsertprocDJSONCommand.Parameters[1].Value.SetNull
  else
  begin
    FMarshal := TDSRestCommand(FInsertprocDJSONCommand.Parameters[1].ConnectionHandler).GetJSONMarshaler;
    try
      FInsertprocDJSONCommand.Parameters[1].Value.SetJSONValue(FMarshal.Marshal(Registros), True);
      if FInstanceOwner then
        Registros.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FInsertprocDJSONCommand.Execute(ARequestFilter);
  Result := FInsertprocDJSONCommand.Parameters[2].Value.GetInt32;
end;

function TServerMethodClient.BuscaJS(query: string; const ARequestFilter: string): TJSONArray;
begin
  if FBuscaJSCommand = nil then
  begin
    FBuscaJSCommand := FConnection.CreateCommand;
    FBuscaJSCommand.RequestType := 'GET';
    FBuscaJSCommand.Text := 'TServerMethod.BuscaJS';
    FBuscaJSCommand.Prepare(TServerMethod_BuscaJS);
  end;
  FBuscaJSCommand.Parameters[0].Value.SetWideString(query);
  FBuscaJSCommand.Execute(ARequestFilter);
  Result := TJSONArray(FBuscaJSCommand.Parameters[1].Value.GetJSONValue(FInstanceOwner));
end;

function TServerMethodClient.BuscaJS_Cache(query: string; const ARequestFilter: string): IDSRestCachedJSONArray;
begin
  if FBuscaJSCommand_Cache = nil then
  begin
    FBuscaJSCommand_Cache := FConnection.CreateCommand;
    FBuscaJSCommand_Cache.RequestType := 'GET';
    FBuscaJSCommand_Cache.Text := 'TServerMethod.BuscaJS';
    FBuscaJSCommand_Cache.Prepare(TServerMethod_BuscaJS_Cache);
  end;
  FBuscaJSCommand_Cache.Parameters[0].Value.SetWideString(query);
  FBuscaJSCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedJSONArray.Create(FBuscaJSCommand_Cache.Parameters[1].Value.GetString);
end;

function TServerMethodClient.InsertJS(Tabla: string; Registro: TJSONArray; const ARequestFilter: string): string;
begin
  if FInsertJSCommand = nil then
  begin
    FInsertJSCommand := FConnection.CreateCommand;
    FInsertJSCommand.RequestType := 'POST';
    FInsertJSCommand.Text := 'TServerMethod."InsertJS"';
    FInsertJSCommand.Prepare(TServerMethod_InsertJS);
  end;
  FInsertJSCommand.Parameters[0].Value.SetWideString(Tabla);
  FInsertJSCommand.Parameters[1].Value.SetJSONValue(Registro, FInstanceOwner);
  FInsertJSCommand.Execute(ARequestFilter);
  Result := FInsertJSCommand.Parameters[2].Value.GetWideString;
end;

function TServerMethodClient.BorrarRegistro(Tabla: string; Where: string; const ARequestFilter: string): string;
begin
  if FBorrarRegistroCommand = nil then
  begin
    FBorrarRegistroCommand := FConnection.CreateCommand;
    FBorrarRegistroCommand.RequestType := 'GET';
    FBorrarRegistroCommand.Text := 'TServerMethod.BorrarRegistro';
    FBorrarRegistroCommand.Prepare(TServerMethod_BorrarRegistro);
  end;
  FBorrarRegistroCommand.Parameters[0].Value.SetWideString(Tabla);
  FBorrarRegistroCommand.Parameters[1].Value.SetWideString(Where);
  FBorrarRegistroCommand.Execute(ARequestFilter);
  Result := FBorrarRegistroCommand.Parameters[2].Value.GetWideString;
end;

function TServerMethodClient.BuscaDJS(query: string; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FBuscaDJSCommand = nil then
  begin
    FBuscaDJSCommand := FConnection.CreateCommand;
    FBuscaDJSCommand.RequestType := 'GET';
    FBuscaDJSCommand.Text := 'TServerMethod.BuscaDJS';
    FBuscaDJSCommand.Prepare(TServerMethod_BuscaDJS);
  end;
  FBuscaDJSCommand.Parameters[0].Value.SetWideString(query);
  FBuscaDJSCommand.Execute(ARequestFilter);
  if not FBuscaDJSCommand.Parameters[1].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FBuscaDJSCommand.Parameters[1].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FBuscaDJSCommand.Parameters[1].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FBuscaDJSCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethodClient.BuscaDJS_Cache(query: string; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FBuscaDJSCommand_Cache = nil then
  begin
    FBuscaDJSCommand_Cache := FConnection.CreateCommand;
    FBuscaDJSCommand_Cache.RequestType := 'GET';
    FBuscaDJSCommand_Cache.Text := 'TServerMethod.BuscaDJS';
    FBuscaDJSCommand_Cache.Prepare(TServerMethod_BuscaDJS_Cache);
  end;
  FBuscaDJSCommand_Cache.Parameters[0].Value.SetWideString(query);
  FBuscaDJSCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FBuscaDJSCommand_Cache.Parameters[1].Value.GetString);
end;

function TServerMethodClient.activar_device(deviceid: string; const ARequestFilter: string): string;
begin
  if Factivar_deviceCommand = nil then
  begin
    Factivar_deviceCommand := FConnection.CreateCommand;
    Factivar_deviceCommand.RequestType := 'GET';
    Factivar_deviceCommand.Text := 'TServerMethod.activar_device';
    Factivar_deviceCommand.Prepare(TServerMethod_activar_device);
  end;
  Factivar_deviceCommand.Parameters[0].Value.SetWideString(deviceid);
  Factivar_deviceCommand.Execute(ARequestFilter);
  Result := Factivar_deviceCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethodClient.InsertDevice(deviceid: string; const ARequestFilter: string): string;
begin
  if FInsertDeviceCommand = nil then
  begin
    FInsertDeviceCommand := FConnection.CreateCommand;
    FInsertDeviceCommand.RequestType := 'GET';
    FInsertDeviceCommand.Text := 'TServerMethod.InsertDevice';
    FInsertDeviceCommand.Prepare(TServerMethod_InsertDevice);
  end;
  FInsertDeviceCommand.Parameters[0].Value.SetWideString(deviceid);
  FInsertDeviceCommand.Execute(ARequestFilter);
  Result := FInsertDeviceCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethodClient.wssendmessage(comp_id: Integer; suc_id: Integer; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FwssendmessageCommand = nil then
  begin
    FwssendmessageCommand := FConnection.CreateCommand;
    FwssendmessageCommand.RequestType := 'GET';
    FwssendmessageCommand.Text := 'TServerMethod.wssendmessage';
    FwssendmessageCommand.Prepare(TServerMethod_wssendmessage);
  end;
  FwssendmessageCommand.Parameters[0].Value.SetInt32(comp_id);
  FwssendmessageCommand.Parameters[1].Value.SetInt32(suc_id);
  FwssendmessageCommand.Execute(ARequestFilter);
  if not FwssendmessageCommand.Parameters[2].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FwssendmessageCommand.Parameters[2].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FwssendmessageCommand.Parameters[2].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FwssendmessageCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethodClient.wssendmessage_Cache(comp_id: Integer; suc_id: Integer; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FwssendmessageCommand_Cache = nil then
  begin
    FwssendmessageCommand_Cache := FConnection.CreateCommand;
    FwssendmessageCommand_Cache.RequestType := 'GET';
    FwssendmessageCommand_Cache.Text := 'TServerMethod.wssendmessage';
    FwssendmessageCommand_Cache.Prepare(TServerMethod_wssendmessage_Cache);
  end;
  FwssendmessageCommand_Cache.Parameters[0].Value.SetInt32(comp_id);
  FwssendmessageCommand_Cache.Parameters[1].Value.SetInt32(suc_id);
  FwssendmessageCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FwssendmessageCommand_Cache.Parameters[2].Value.GetString);
end;

function TServerMethodClient.sp_wmdelphi(comp_id: Integer; suc_id: Integer; key_from_me: Integer; message_id: Integer; key_remote_jid: string; data: string; media_wa_type: string; media_name: string; media_caption: string; raw_data: TStream; timestamp: TDateTime; lat: Double; lon: Double; remote_resource: string; const ARequestFilter: string): Integer;
begin
  if Fsp_wmdelphiCommand = nil then
  begin
    Fsp_wmdelphiCommand := FConnection.CreateCommand;
    Fsp_wmdelphiCommand.RequestType := 'POST';
    Fsp_wmdelphiCommand.Text := 'TServerMethod."sp_wmdelphi"';
    Fsp_wmdelphiCommand.Prepare(TServerMethod_sp_wmdelphi);
  end;
  Fsp_wmdelphiCommand.Parameters[0].Value.SetInt32(comp_id);
  Fsp_wmdelphiCommand.Parameters[1].Value.SetInt32(suc_id);
  Fsp_wmdelphiCommand.Parameters[2].Value.SetInt32(key_from_me);
  Fsp_wmdelphiCommand.Parameters[3].Value.SetInt32(message_id);
  Fsp_wmdelphiCommand.Parameters[4].Value.SetWideString(key_remote_jid);
  Fsp_wmdelphiCommand.Parameters[5].Value.SetWideString(data);
  Fsp_wmdelphiCommand.Parameters[6].Value.SetWideString(media_wa_type);
  Fsp_wmdelphiCommand.Parameters[7].Value.SetWideString(media_name);
  Fsp_wmdelphiCommand.Parameters[8].Value.SetWideString(media_caption);
  Fsp_wmdelphiCommand.Parameters[9].Value.SetStream(raw_data, FInstanceOwner);
  Fsp_wmdelphiCommand.Parameters[10].Value.AsDateTime := timestamp;
  Fsp_wmdelphiCommand.Parameters[11].Value.SetDouble(lat);
  Fsp_wmdelphiCommand.Parameters[12].Value.SetDouble(lon);
  Fsp_wmdelphiCommand.Parameters[13].Value.SetWideString(remote_resource);
  Fsp_wmdelphiCommand.Execute(ARequestFilter);
  Result := Fsp_wmdelphiCommand.Parameters[14].Value.GetInt32;
end;

constructor TServerMethodClient.Create(ARestConnection: TDSRestConnection);
begin
  inherited Create(ARestConnection);
end;

constructor TServerMethodClient.Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ARestConnection, AInstanceOwner);
end;

destructor TServerMethodClient.Destroy;
begin
  FEchoStringCommand.DisposeOf;
  FReverseStringCommand.DisposeOf;
  FInsertprocDJSONCommand.DisposeOf;
  FBuscaJSCommand.DisposeOf;
  FBuscaJSCommand_Cache.DisposeOf;
  FInsertJSCommand.DisposeOf;
  FBorrarRegistroCommand.DisposeOf;
  FBuscaDJSCommand.DisposeOf;
  FBuscaDJSCommand_Cache.DisposeOf;
  Factivar_deviceCommand.DisposeOf;
  FInsertDeviceCommand.DisposeOf;
  FwssendmessageCommand.DisposeOf;
  FwssendmessageCommand_Cache.DisposeOf;
  Fsp_wmdelphiCommand.DisposeOf;
  inherited;
end;

end.

