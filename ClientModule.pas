unit ClientModule;

interface

uses
  System.SysUtils, System.Classes, ClientClasses, Datasnap.DSClientRest;

type
  TClientModule1 = class(TDataModule)
    DSRestConnection1: TDSRestConnection;
  private
    FInstanceOwner: Boolean;
    FServerMethodClient: TServerMethodClient;
    function GetServerMethodClient: TServerMethodClient;
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property InstanceOwner: Boolean read FInstanceOwner write FInstanceOwner;
    property ServerMethodClient: TServerMethodClient read GetServerMethodClient write FServerMethodClient;

end;

var
  ClientModule1: TClientModule1;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

{$R *.dfm}

constructor TClientModule1.Create(AOwner: TComponent);
begin
  inherited;
  FInstanceOwner := True;
end;

destructor TClientModule1.Destroy;
begin
  FServerMethodClient.Free;
  inherited;
end;

function TClientModule1.GetServerMethodClient: TServerMethodClient;
begin
  if FServerMethodClient = nil then
    FServerMethodClient:= TServerMethodClient.Create(DSRestConnection1, FInstanceOwner);
  Result := FServerMethodClient;
end;

end.
