unit Principal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Edit,
  FMX.ListBox, FMX.Layouts, FMX.StdCtrls, FMX.Controls.Presentation,
  FireDAC.Comp.Client, Firedac.DApt, FMX.TabControl, Data.FireDACJSONReflect,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  System.Rtti, System.Bindings.Outputs, Fmx.Bind.Editors, Data.Bind.Components,
  Data.Bind.DBScope, FMX.ScrollBox, FMX.Memo, FMX.DateTimeCtrls, FMX.Gestures,
  FMX.MultiView, FMX.Objects, System.ImageList, FMX.ImgList,
  {$IF DEFINED(ANDROID)}
     Androidapi.JNI.Telephony,
     Androidapi.JNI.Provider,
     Androidapi.JNIBridge,
     Androidapi.JNI.GraphicsContentViewText,
     Androidapi.JNI.JavaTypes,
     Androidapi.Helpers,
     Androidapi.Jni.app,
  {$ENDIF}
  FMX.DialogService, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  FMX.Toast
   ;

type
  TFPrincipal = class(TForm)
    ToolBar1: TToolBar;
    BMenu: TButton;
    ListBox1: TListBox;
    ListBoxItem1: TListBoxItem;
    ListBoxItem2: TListBoxItem;
    ListBoxItem3: TListBoxItem;
    Servidor: TEdit;
    basededatos: TEdit;
    ListBoxGroupHeader1: TListBoxGroupHeader;
    ListBoxItem4: TListBoxItem;
    Server: TEdit;
    ListBoxGroupHeader2: TListBoxGroupHeader;
    ListBoxItem5: TListBoxItem;
    Port: TEdit;
    Label1: TLabel;
    TabControl1: TTabControl;
    Estados: TTabItem;
    Conexiones: TTabItem;
    MemfixedRules: TFDMemTable;
    ListBox2: TListBox;
    ToolBar2: TToolBar;
    Button3: TButton;
    Button4: TButton;
    ListBoxGroupHeader3: TListBoxGroupHeader;
    ListBoxItem7: TListBoxItem;
    ListBoxItem10: TListBoxItem;
    ListBoxItem11: TListBoxItem;
    ListBoxItem12: TListBoxItem;
    ListBoxItem13: TListBoxItem;
    ListBoxItem14: TListBoxItem;
    ListBoxItem15: TListBoxItem;
    ListBoxItem16: TListBoxItem;
    ListBoxItem17: TListBoxItem;
    ListBoxItem18: TListBoxItem;
    ListBoxItem19: TListBoxItem;
    ListBoxItem21: TListBoxItem;
    ListBoxItem22: TListBoxItem;
    ListBoxItem23: TListBoxItem;
    ListBoxItem24: TListBoxItem;
    ListBoxItem25: TListBoxItem;
    ListBoxItem26: TListBoxItem;
    ListBoxItem27: TListBoxItem;
    ListBoxItem8: TListBoxItem;
    ListBoxItem9: TListBoxItem;
    ListBoxItem28: TListBoxItem;
    ListBoxItem29: TListBoxItem;
    ListBoxItem30: TListBoxItem;
    ListBoxItem31: TListBoxItem;
    MemHost: TFDMemTable;
    ser_no: TEdit;
    comp_id: TEdit;
    suc_id: TEdit;
    device_phone_number: TEdit;
    incoming_text: TEdit;
    search_atbegin: TEdit;
    search_atend: TEdit;
    search_inbetween: TEdit;
    procedure_name: TEdit;
    wait_minutes: TEdit;
    receiver: TEdit;
    specific_contacts: TEdit;
    specific_groups: TEdit;
    specific_hours: TEdit;
    specific_days: TEdit;
    active: TEdit;
    begin_date: TEdit;
    media_url: TEdit;
    MEDIA_NAME: TEdit;
    START_OUT_SERVICE_TIME: TEdit;
    END_OUT_SERVICE_TIME: TEdit;
    end_date: TEdit;
    add_date: TEdit;
    send_text: TEdit;
    BindingsList1: TBindingsList;
    BindSourceDB2: TBindSourceDB;
    Button6: TButton;
    GestureManager1: TGestureManager;
    MultiView1: TMultiView;
    ListBox3: TListBox;
    ListBoxItem20: TListBoxItem;
    Image1: TImage;
    ListBoxItem32: TListBoxItem;
    ListBoxItem33: TListBoxItem;
    ListBoxItem34: TListBoxItem;
    MemfixedRulescomp_id: TIntegerField;
    MemfixedRulessuc_id: TIntegerField;
    MemfixedRulesdevice_phone_number: TStringField;
    MemfixedRulesincoming_text: TStringField;
    MemfixedRulessearch_atbegin: TBooleanField;
    MemfixedRulessearch_atend: TBooleanField;
    MemfixedRulessearch_inbetween: TBooleanField;
    MemfixedRulessend_text: TMemoField;
    MemfixedRulesprocedure_name: TStringField;
    MemfixedRuleswait_minutes: TIntegerField;
    MemfixedRulesreceiver: TStringField;
    MemfixedRulesspecific_contacts: TMemoField;
    MemfixedRulesspecific_groups: TMemoField;
    MemfixedRulesspecific_hours: TMemoField;
    MemfixedRulesspecific_days: TMemoField;
    MemfixedRulesactive: TBooleanField;
    MemfixedRulesbegin_date: TSQLTimeStampField;
    MemfixedRulesend_date: TSQLTimeStampField;
    MemfixedRulesadd_date: TSQLTimeStampField;
    MemfixedRulesmedia_url: TStringField;
    MemfixedRulesMEDIA_NAME: TStringField;
    MemfixedRulesSTART_OUT_SERVICE_TIME: TTimeField;
    MemfixedRulesEND_OUT_SERVICE_TIME: TTimeField;
    BindSourceDB1: TBindSourceDB;
    LinkControlToField1: TLinkControlToField;
    LinkControlToField2: TLinkControlToField;
    LinkControlToField3: TLinkControlToField;
    LinkControlToField4: TLinkControlToField;
    LinkControlToField8: TLinkControlToField;
    LinkControlToField9: TLinkControlToField;
    LinkControlToField10: TLinkControlToField;
    LinkControlToField11: TLinkControlToField;
    LinkControlToField12: TLinkControlToField;
    LinkControlToField13: TLinkControlToField;
    LinkControlToField14: TLinkControlToField;
    LinkControlToField15: TLinkControlToField;
    LinkControlToField16: TLinkControlToField;
    LinkControlToField17: TLinkControlToField;
    LinkControlToField18: TLinkControlToField;
    LinkControlToField19: TLinkControlToField;
    LinkControlToField20: TLinkControlToField;
    LinkControlToField21: TLinkControlToField;
    LinkControlToField22: TLinkControlToField;
    LinkControlToField23: TLinkControlToField;
    LinkControlToField24: TLinkControlToField;
    LinkControlToField25: TLinkControlToField;
    LinkControlToField26: TLinkControlToField;
    LinkControlToField27: TLinkControlToField;
    Button5: TButton;
    ImageList1: TImageList;
    Button1: TButton;
    ListBoxItem35: TListBoxItem;
    deviceid: TEdit;
    ListBoxGroupHeader4: TListBoxGroupHeader;
    ListBoxItem36: TListBoxItem;
    MComp_id: TEdit;
    ListBoxItem37: TListBoxItem;
    MSuc_id: TEdit;
    ListBoxItem38: TListBoxItem;
    Time_scan: TEdit;
    ListBoxItem39: TListBoxItem;
    BotonX: TEdit;
    ListBoxItem40: TListBoxItem;
    BotonY: TEdit;
    ListBoxItem41: TListBoxItem;
    phone: TEdit;
    ListBoxGroupHeader5: TListBoxGroupHeader;
    ListBoxItem6: TListBoxItem;
    messageid: TEdit;
    MemDevices: TFDMemTable;
    LinkControlToField6: TLinkControlToField;
    LinkControlToField28: TLinkControlToField;
    BindSourceDB3: TBindSourceDB;
    LinkControlToField29: TLinkControlToField;
    LinkControlToField32: TLinkControlToField;
    LinkControlToField33: TLinkControlToField;
    LinkControlToField34: TLinkControlToField;
    LinkControlToField35: TLinkControlToField;
    LinkControlToField36: TLinkControlToField;
    Button2: TButton;
    UltCambio: TEdit;
    LinkControlToField5: TLinkControlToField;
    MemHostservidor: TStringField;
    MemHostbasededatos: TStringField;
    MemHostUltCambio: TSQLTimeStampField;
    MemfixedRulesser_no: TIntegerField;
    ToolBar3: TToolBar;
    Button7: TButton;
    MemRestdb: TFDMemTable;
    MemRestdbserver: TStringField;
    MemRestdbport: TIntegerField;
    MemRestdbactivado: TBooleanField;
    CheckBox1: TCheckBox;
    BindSourceDB4: TBindSourceDB;
    LinkControlToField30: TLinkControlToField;
    LinkControlToField31: TLinkControlToField;
    LinkControlToField37: TLinkControlToField;
    VertScrollBox1: TVertScrollBox;
    MainLayout1: TLayout;
    LinkControlToField7: TLinkControlToField;
    MemDevicesser_no: TIntegerField;
    MemDevicescomp_id: TIntegerField;
    MemDevicessuc_id: TIntegerField;
    MemDevicesdevice_id: TStringField;
    MemDevicesdevice_phone: TStringField;
    MemDevicesemail: TStringField;
    MemDevicesactivation_key: TStringField;
    MemDevicesdevice_enabled: TBooleanField;
    MemDevicestime_scan: TIntegerField;
    MemDevicesbotonx: TIntegerField;
    MemDevicesbotony: TIntegerField;
    MemDevicesmessageid: TIntegerField;
    MemDevicesadd_date: TSQLTimeStampField;
    MemDevicesregistration_date: TSQLTimeStampField;
    IdTCPClient1: TIdTCPClient;
    ListBoxItem42: TListBoxItem;
    email: TEdit;
    BindSourceDB5: TBindSourceDB;
    LinkControlToField38: TLinkControlToField;
    FMXToast1: TFMXToast;
    Label2: TLabel;
    WhatsAppSend: TTabItem;
    VertScrollBox2: TVertScrollBox;
    ListBoxItem43: TListBoxItem;
    ToolBar4: TToolBar;
    Button8: TButton;
    Button9: TButton;
    Label3: TLabel;
    ListBox4: TListBox;
    ListBoxItem44: TListBoxItem;
    ListBoxItem45: TListBoxItem;
    ListBoxItem46: TListBoxItem;
    ListBoxItem47: TListBoxItem;
    ListBoxItem49: TListBoxItem;
    ListBoxItem50: TListBoxItem;
    ListBoxItem51: TListBoxItem;
    ListBoxItem52: TListBoxItem;
    ListBoxItem53: TListBoxItem;
    username1: TEdit;
    raw_data1: TEdit;
    media_name1: TEdit;
    ser_no1: TEdit;
    remote_resource1: TEdit;
    media_type1: TEdit;
    lat1: TEdit;
    long1: TEdit;
    ListBoxGroupHeader6: TListBoxGroupHeader;
    MemWhatsappSend: TFDMemTable;
    MemWhatsappSendmessage: TWideMemoField;
    MemWhatsappSendusername: TWideStringField;
    MemWhatsappSendraw_data: TBlobField;
    MemWhatsappSendmedia_name: TMemoField;
    MemWhatsappSendser_no: TFDAutoIncField;
    MemWhatsappSendremote_resource: TStringField;
    MemWhatsappSendmedia_type: TWideStringField;
    MemWhatsappSendlat: TFMTBCDField;
    MemWhatsappSendlong: TFMTBCDField;
    ListBoxItem54: TListBoxItem;
    Button10: TButton;
    BindSourceDB6: TBindSourceDB;
    LinkControlToField40: TLinkControlToField;
    LinkControlToField41: TLinkControlToField;
    LinkControlToField42: TLinkControlToField;
    LinkControlToField43: TLinkControlToField;
    LinkControlToField44: TLinkControlToField;
    LinkControlToField45: TLinkControlToField;
    LinkControlToField46: TLinkControlToField;
    Message1: TMemo;
    LinkControlToField47: TLinkControlToField;
    procedure Button1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListBoxItem34Click(Sender: TObject);
    procedure ListBoxItem32Click(Sender: TObject);
    procedure ListBoxItem33Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure FormVirtualKeyboardHidden(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormVirtualKeyboardShown(Sender: TObject;
      KeyboardVisible: Boolean; const Bounds: TRect);
    procedure FormFocusChanged(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBoxItem43Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
  private
    FKBBounds: TRectF;
    FNeedOffset: Boolean;
    { Private declarations }
    procedure CalcContentBoundsProc(Sender: TObject;
                                    var ContentBounds: TRectF);
    procedure RestorePosition;
    procedure UpdateKBBounds;
    procedure refrescar;
    procedure llenar;
    procedure guardarmem(tablein : tfdmemtable; tableout : tfdquery);
   {$IF DEFINED(ANDROID)}
    procedure deviceidphone;
   {$ENDIF}
    function isconnected:boolean;
  public
    serno : string;
    { Public declarations }
  end;

var
  FPrincipal: TFPrincipal;

implementation

{$R *.fmx}

uses Data, ClientModule, System.Math;

procedure TFPrincipal.Button10Click(Sender: TObject);
var dataset: TFDJSONDataSets;
begin
  {TDialogService.MessageDialog('Desea procesar Whatsapp Send Message? ', TMsgDlgType.mtConfirmation,
    [ TMsgDlgBtn.mbYes, TMsgDlgBtn.mbCancel ], TMsgDlgBtn.mbCancel, 0,
    procedure(const AResult: TModalResult)
    begin
      case AResult of
        mrYes :
         begin        }
        //    try
              dataset := ClientModule1.ServerMethodClient.wssendmessage(78, 1 );
              MemWhatsappSend.Active := false;
              MemWhatsappSend.AppendData(TFDJSONDataSetsReader.GetListValue(dataset, 0));
       //     finally
              MemWhatsappSend.Active := true;
              label3.Text :=  MemWhatsappSend.RecNo.ToString + ' de '+MemWhatsappSend.RecordCount.ToString;
      //      end;
      {   end;
      end;
    end);  }
end;

procedure TFPrincipal.Button1Click(Sender: TObject);
begin
 TDialogService.MessageDialog('Desea leer registros de la nube? ', TMsgDlgType.mtConfirmation,
    [ TMsgDlgBtn.mbYes, TMsgDlgBtn.mbCancel ], TMsgDlgBtn.mbCancel, 0,
    procedure(const AResult: TModalResult)
    begin
      case AResult of
        mrYes :  begin
               refrescar;
               Button6.Visible := true;
             end;
      end;
    end);
end;

procedure TFPrincipal.Button2Click(Sender: TObject);
var dataset: TFDJSONDataSets;
    sqlquery : string;
begin
  TDialogService.MessageDialog('Actualizar el Dispositivo? ', TMsgDlgType.mtConfirmation,
   [ TMsgDlgBtn.mbYes, TMsgDlgBtn.mbCancel ], TMsgDlgBtn.mbCancel, 0,
    procedure(const AResult: TModalResult)
    begin
      case AResult of
        mrYes :  begin
                   if deviceid.Text <> '' then
                    begin

                     serno := ClientModule1.ServerMethodClient.InsertDevice(deviceid.Text);
                    if serno <> '' then
                       try
                       SqlQuery := 'select * from devices where ser_no = '+serno;
                       dataset := ClientModule1.ServerMethodClient.buscadjs(SqlQuery);
                       Memdevices.Active := false;
                       MemDevices.AppendData(TFDJSONDataSetsReader.GetListValue(dataset, 0));
                       finally
                         MemDevices.Active := true;
                       end;
                   {  if not memdevices.IsEmpty then
                         begin
                          dm.Qutils.sql.Text := 'delete from devices';
                          dm.Qutils.ExecSQL;
                          guardarmem(MemDevices, dm.devicesTable);
                         end;}
                    end;

                 end;

      end;
    end);

end;

procedure TFPrincipal.Button3Click(Sender: TObject);
begin
  MemFixedRules.Prior;
  label2.Text :=  MemFixedRules.RecNo.ToString + ' de '+memFixedRules.RecordCount.ToString;
end;

procedure TFPrincipal.Button4Click(Sender: TObject);
begin
  MemFixedRules.next;
  label2.Text :=  MemFixedRules.RecNo.ToString + ' de '+memFixedRules.RecordCount.ToString;
end;

procedure TFPrincipal.Button5Click(Sender: TObject);
begin
   {$IF DEFINED(ANDROID)}
    deviceidphone;
   {$ENDIF}
  llenar;
end;

procedure TFPrincipal.Button6Click(Sender: TObject);
begin
  case TabControl1.TabIndex of
    0 : begin
         TDialogService.MessageDialog('Desea Guardar Fixed Rules? ', TMsgDlgType.mtConfirmation,
         [ TMsgDlgBtn.mbYes, TMsgDlgBtn.mbCancel ], TMsgDlgBtn.mbCancel, 0,
          procedure(const AResult: TModalResult)
          begin
            case AResult of
              mrYes : begin
                   dm.Qutils.sql.Text := 'delete from fixedrules';
                      dm.Qutils.ExecSQL;
                   guardarmem(MemFixedRules, dm.FixedrulesTable);

                   FMXToast1.ToastMessage := 'Fixed Rules Guardada con éxito';
                   FMXToast1.Show(Self);

                   Button6.Visible := false;
                  end;
            end;
          end);

        end;
   1 : begin
        TDialogService.MessageDialog('Desea Guardar conexiones? ', TMsgDlgType.mtConfirmation,
          [ TMsgDlgBtn.mbYes, TMsgDlgBtn.mbCancel ], TMsgDlgBtn.mbCancel, 0,
          procedure(const AResult: TModalResult)
          begin
            case AResult of
              mrYes :  begin
                         Button6.Visible := false;

                         dm.Qutils.sql.Text := 'delete from host';
                         dm.Qutils.ExecSQL;
                         guardarmem(MemHost, dm.HostTable);

                         dm.Qutils.sql.Text := 'delete from restdb';
                         dm.Qutils.ExecSQL;
                         guardarmem(MemRestdb, dm.RestdbTable);

                         dm.Qutils.sql.Text := 'delete from devices';
                         dm.Qutils.ExecSQL;
                         guardarmem(MemDevices, dm.devicesTable);

                         FMXToast1.ToastMessage := 'host y devices Guardadas con éxito';
                         FMXToast1.Show(Self);

                         llenar;
                       end;
            end;
          end);

       end;
  end;
end;

procedure TFPrincipal.Button7Click(Sender: TObject);
var activation_key : string;
begin
  if deviceid.Text = '' then
     deviceid.Text := '357957051963262';

   activation_key := ClientModule1.ServerMethodClient.activar_device(deviceid.text);

   if activation_key = deviceid.Text then
      begin
       CheckBox1.IsChecked := true;
       dm.Qutils.sql.Text := 'delete from restdb';
       dm.Qutils.ExecSQL;
       dm.RestdbTable.open;
       MemRestdb.open;
       MemRestdb.Edit;
       MemRestdbactivado.Value := true;
       MemRestdb.Post;
       guardarmem(MemRestdb, dm.RestdbTable);
       FMXToast1.ToastMessage := 'dispositivo activado correctamente';
       FMXToast1.Show(Self);
       CheckBox1.IsChecked := true;
      end
     else
      begin
       FMXToast1.ToastMessage := 'dispositivo no tiene activación creada';
       FMXToast1.Show(Self);
      end;

   Button7.Enabled := not (CheckBox1.IsChecked);
end;

procedure TFPrincipal.Button8Click(Sender: TObject);
begin
  MemWhatsappSend.Prior;
  label3.Text :=  MemWhatsappSend.RecNo.ToString + ' de '+MemWhatsappSend.RecordCount.ToString;
end;

procedure TFPrincipal.Button9Click(Sender: TObject);
begin
  MemWhatsappSend.next;
  label3.Text :=  MemWhatsappSend.RecNo.ToString + ' de '+MemWhatsappSend.RecordCount.ToString;
end;

procedure TFPrincipal.CalcContentBoundsProc(Sender: TObject;
  var ContentBounds: TRectF);
begin
   if FNeedOffset and (FKBBounds.Top > 0) then
  begin
    ContentBounds.Bottom := Max(ContentBounds.Bottom,
                                2 * ClientHeight - FKBBounds.Top);
  end;
end;

{$IF DEFINED(ANDROID)}
procedure TFPrincipal.deviceidphone;
var obj : JObject;
     tm : JTelephonyManager;
identifier, phonenumber : String;
begin
  obj:=  TAndroidHelper.Context.getSystemService(TJContext.JavaClass.TELEPHONY_SERVICE);
  if obj <>nil then
     begin
      tm:= TJTelephonyManager.Wrap( (obj as ILocalObject).GetObjectID );
      if tm <> nil then
        begin
         identifier  := JStringToString(tm.getDeviceId);
         phonenumber := JStringToString(tm.getLine1Number);
        end;
     end;
     if identifier = '' then
        identifier:= JStringToString(TJSettings_Secure.JavaClass.getString(TAndroidHelper.Activity.getContentResolver,TJSettings_Secure.JavaClass.ANDROID_ID));

       deviceid.text := identifier;
       phone.text    := phonenumber;
 end;
{$ENDIF}


procedure TFPrincipal.FormCreate(Sender: TObject);
begin
  VKAutoShowMode := TVKAutoShowMode.Always;
  VertScrollBox1.OnCalcContentBounds := CalcContentBoundsProc;
end;

procedure TFPrincipal.FormFocusChanged(Sender: TObject);
begin
  UpdateKBBounds;
end;

procedure TFPrincipal.FormShow(Sender: TObject);
begin
   MultiView1.Mode := TMultiViewMode.drawer;
   Button6.Visible := false;
   tabcontrol1.TabPosition := TTabPosition.None;
   tabcontrol1.TabIndex := 1;
   {$IF DEFINED(ANDROID)}
    deviceidphone;
   {$ENDIF}
   llenar;
end;

procedure TFPrincipal.FormVirtualKeyboardHidden(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  FKBBounds.Create(0, 0, 0, 0);
  FNeedOffset := False;
  RestorePosition;
end;

procedure TFPrincipal.FormVirtualKeyboardShown(Sender: TObject;
  KeyboardVisible: Boolean; const Bounds: TRect);
begin
  FKBBounds := TRectF.Create(Bounds);
  FKBBounds.TopLeft := ScreenToClient(FKBBounds.TopLeft);
  FKBBounds.BottomRight := ScreenToClient(FKBBounds.BottomRight);
  UpdateKBBounds;
end;

procedure TFPrincipal.guardarmem(tablein : tfdmemtable; tableout : tfdquery);
var i : integer;
  campo : string;
  Field : TField;
begin
  tablein.first;
  while not tablein.eof do
   begin
    if not tableout.active then
       tableout.open;
    tableout.Append;
     for i := 0 to tableout.FieldDefs.Count -1 do
      begin
        campo := tableout.FieldDefs[i].Name;
        Field := tablein.FindField(campo) ;
       if Field <> nil then
        tableout.FieldByName(campo).value := tablein.FieldByName(campo).value;
      end;
    tableout.post;
    tablein.next;
   end;
   tableout.open;

end;

function TFPrincipal.isconnected: boolean;
begin
    result:=false;
    try
      IdTCPClient1.ReadTimeout:=2000;
      IdTCPClient1.ConnectTimeout:=2000;
      IdTCPClient1.Port := StrToIntDef(Port.Text, 80);
      IdTCPClient1.Host := server.Text;
      IdTCPClient1.Connect;
      IdTCPClient1.Disconnect;
     result:=true;
    except
     result:=false;
    end;

end;

procedure TFPrincipal.ListBoxItem32Click(Sender: TObject);
begin
   tabcontrol1.TabIndex := 0;
   multiview1.HideMaster;
   llenar;
end;

procedure TFPrincipal.ListBoxItem33Click(Sender: TObject);
begin
  tabcontrol1.TabIndex := 1;
  multiview1.HideMaster;
end;

procedure TFPrincipal.ListBoxItem34Click(Sender: TObject);
begin
   dm.DbConnection.Close;
   close;
end;

procedure TFPrincipal.ListBoxItem43Click(Sender: TObject);
begin
   tabcontrol1.TabIndex := 2;
   multiview1.HideMaster;
end;

procedure TFPrincipal.llenar;
var Qry : tfdQuery;
begin
   qry := tfdquery.create(nil);
   qry.connection := dm.DBConnection;

    qry.open('select * from restdb');
    MemRestdb.Active := true;
    MemRestdb.CopyDataSet(qry,[coAppend]);
    MemRestdb.open;

    if server.Text = '' then
       begin
         server.Text :=  '142.44.139.4';
         port.text := '61703';
       end;

 case tabcontrol1.TabIndex of
   1 : begin
         qry.open('select * from host');
         MemHost.Active := true;
         MemHost.CopyDataSet(qry,[coAppend]);
         MemHost.open;

         qry.open('select * from devices');
         memdevices.Active := true;
         memdevices.CopyDataSet(qry,[ coAppend ]);
         memdevices.open;
       end;
   0 : begin
        qry.Open('select * from fixedRules');
        MemFixedRules.Active := true;
        MemFixedRules.CopyDataSet(qry,[coAppend]);
        MemFixedRules.open;
        label2.Text :=  MemFixedRules.RecNo.ToString + ' de '+memFixedRules.RecordCount.ToString;
       end;
 end;
   qry.Free;
end;

procedure TFPrincipal.refrescar;
var dataset: TFDJSONDataSets;
    SqlQuery : string;
    Qry : tfdQuery;
begin
   qry := tfdquery.create(nil);
   qry.connection := dm.DBConnection;

    qry.open('select * from restdb');
    MemRestdb.Active := true;
    MemRestdb.CopyDataSet(qry,[coAppend]);
    MemRestdb.open;

    if server.Text = '' then
       begin
         server.Text :=  '142.44.139.4';
         port.text := '61703';
       end;

case tabcontrol1.TabIndex of
   1 : begin
         SqlQuery := 'select * from host';
         dataset := ClientModule1.ServerMethodClient.buscadjs(SqlQuery);
         MemHost.Active := false;
         MemHost.AppendData(TFDJSONDataSetsReader.GetListValue(dataset, 0));
         MemHost.Active := true;

         {$IF DEFINED(ANDROID)}
         deviceidphone;
         {$ENDIF}

         SqlQuery := 'select * from devices where device_id = '+QuotedStr(deviceid.Text);

         dataset := ClientModule1.ServerMethodClient.buscadjs(SqlQuery);
         Memdevices.Active := false;
         MemDevices.AppendData(TFDJSONDataSetsReader.GetListValue(dataset, 0));
         MemDevices.Active := true;

        FMXToast1.ToastMessage := 'Host y devices data leida de la nube';
        FMXToast1.Show(Self);
       end;
   0 : begin

         SqlQuery := 'select * from fixedRules where device_phone_number = '+QuotedStr(phone.Text);
         dataset := ClientModule1.ServerMethodClient.buscadjs(SqlQuery);
         MemFixedRules.Active := false;
         MemFixedRules.AppendData(TFDJSONDataSetsReader.GetListValue(dataset, 0));
         MemFixedRules.Active := true;

         label2.Text :=  MemFixedRules.RecNo.ToString + ' de '+memFixedRules.RecordCount.ToString;
         if not MemFixedRules.IsEmpty then
           begin
            FMXToast1.ToastMessage := 'FixedRules data leida de la nube';
            FMXToast1.Show(Self);
           end;
       end;
end;

end;

procedure TFPrincipal.RestorePosition;
begin
  VertScrollBox1.ViewportPosition := PointF(VertScrollBox1.ViewportPosition.X, 0);
  MainLayout1.Align := TAlignLayout.Client;
  VertScrollBox1.RealignContent;
end;

procedure TFPrincipal.UpdateKBBounds;
var
  LFocused : TControl;
  LFocusRect: TRectF;
begin
  FNeedOffset := False;
  if Assigned(Focused) then
  begin
    LFocused := TControl(Focused.GetObject);
    LFocusRect := LFocused.AbsoluteRect;
    LFocusRect.Offset(VertScrollBox1.ViewportPosition);
    if (LFocusRect.IntersectsWith(TRectF.Create(FKBBounds))) and
       (LFocusRect.Bottom > FKBBounds.Top) then
    begin
      FNeedOffset := True;
      MainLayout1.Align := TAlignLayout.Horizontal;
      VertScrollBox1.RealignContent;
      Application.ProcessMessages;
      VertScrollBox1.ViewportPosition :=
        PointF(VertScrollBox1.ViewportPosition.X,
               LFocusRect.Bottom - FKBBounds.Top);
    end;
  end;
  if not FNeedOffset then
    RestorePosition;

end;

end.
